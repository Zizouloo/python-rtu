# main.py - Interface principale

## Dépendances

- websocket-client

## Détails implémentation

- Toute l'interface est codée avec tkinter avec un canvas (= je dessine les composants)
- C'est de la prog. objet : une classe principale RtuApp qui contient notamment toutes les données et qui se charge d'instancier les différentes "pages" et passer de l'un à l'autre. En fait toutes les pages sont surperposées et le changement de page ramène la page sélectionnée à l'avant.
- Pour rajouter une page : nouvelle classe sur le modèle de HomePage ou ComPage. **Important :** il faut absolument avoir une méthode `init_canvas` chargée de dessiner la page. Et il faut rajouter la classe dans la boucle du constructeur de RtuApp
- Pour la websocket : un thread est démarré pour faire la requête histoire de pas bloquer l'interface le temps de l'échange

# server.py - Exemple de serveur websocket

## Dépendances

- websocket-server

## Détails implémentation

- La méthode importante est `set_fn_message_received` qui permet d'indiquer la méthode à exécuter lors de la réception d'un message
- là c'est un serveur de test qui renvoie au client ce qu'il a envoyé (d'ailleurs j'ai lancé un print côté interface)

