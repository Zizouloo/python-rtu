import logging
from websocket_server import WebsocketServer


def echo(client, s, message):
    s.send_message(client, message)


if __name__ == '__main__':
    server = WebsocketServer(80, host='127.0.0.1', loglevel=logging.INFO)
    server.set_fn_message_received(echo)
    server.run_forever()
