import threading
import tkinter as tk
from websocket import create_connection


class RtuApp(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        self.geometry("480x320")
        self.resizable(False, False)

        self.com_left_value = 123.425
        self.com_right_value = 132.930
        self.nav_left_value = 108.50
        self.nav_center_value = 110.30
        self.nav_right_value = 114.45
        self.adf_left_value = 190.5
        self.adf_right_value = 339.0
        self.atc_left_value = 4232
        self.atc_right_value = 'AUTO'

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (HomePage, ComPage):  # add new pages here
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("HomePage")

    def show_frame(self, page_name):
        frame = self.frames[page_name]
        frame.tkraise()
        frame.init_canvas()

    def send_ws(self):
        ws = create_connection("ws://localhost")
        ws.send(str(self.com_left_value))
        print(ws.recv())
        ws.close()


class HomePage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.canvas = tk.Canvas(self, bg='black', highlightthickness=0, width=800, height=600)
        self.canvas.pack()
        self.com_update_button = None
        self.com_page_button = None
        self.init_canvas()
        self.canvas.pack()

    def init_canvas(self):
        self.canvas.delete("all")
        self.create_row_com()
        self.create_row_nav()
        self.create_row_adf()
        self.create_row_atc()

    def move_to_com_page(self, _):
        self.controller.show_frame("ComPage")

    def update_com_value(self, _):
        self.controller.com_left_value = self.controller.com_right_value
        self.init_canvas()
        threading.Thread(target=self.controller.send_ws, daemon=True).start()

    def create_row_com(self):
        self.canvas.create_rectangle(20, 26, 460, 82, outline='cyan')
        self.canvas.create_text(100, 55, text=self.controller.com_left_value, fill='green', font='-size 25')
        self.com_update_button = self.canvas.create_text(250, 55, text="COM 1", fill='cyan', font='-size 15')
        self.com_page_button = self.canvas.create_text(380, 55, text=self.controller.com_right_value,
                                                       fill='white', font='-size 25')
        self.canvas.tag_bind(self.com_page_button, "<ButtonPress-1>", self.move_to_com_page)
        self.canvas.tag_bind(self.com_update_button, "<ButtonPress-1>", self.update_com_value)

    def create_row_nav(self):
        self.canvas.create_rectangle(20, 95, 460, 151, outline='cyan')
        self.canvas.create_text(100, 125, text=self.controller.nav_left_value, fill='green', font='-size 25')
        self.canvas.create_text(250, 125, text=self.controller.nav_center_value, fill='yellow', font='-size 25')
        self.canvas.create_text(380, 125, text=self.controller.nav_right_value, fill='white', font='-size 25')

    def create_row_adf(self):
        self.canvas.create_rectangle(20, 164, 460, 220, outline='cyan')
        self.canvas.create_text(100, 190, text=self.controller.adf_left_value, fill='green', font='-size 25')
        self.canvas.create_text(250, 190, text="ADF 1", fill='cyan', font='-size 15')
        self.canvas.create_text(380, 190, text=self.controller.adf_right_value, fill='white', font='-size 25')

    def create_row_atc(self):
        self.canvas.create_rectangle(250, 233, 460, 289, outline='cyan')
        self.canvas.create_rectangle(320, 230, 390, 250, fill='black')
        self.canvas.create_text(80, 260, text="< TCAS", fill='white', font='-size 25')
        self.canvas.create_text(300, 260, text=self.controller.atc_left_value, fill='lime', font='-size 22')
        self.canvas.create_text(355, 233, text="ATC 1", fill='cyan', font='-size 15')
        self.canvas.create_text(410, 260, text=self.controller.atc_right_value, fill='cyan', font='-size 22')


class ComPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.canvas = tk.Canvas(self, bg='black', highlightthickness=0, width=480, height=320)
        self.canvas.pack()
        self.return_button = None
        self.edit_button = None
        self.edit_window = None
        self.init_canvas()
        self.canvas.pack()

    def move_to_home_page(self, _):
        self.controller.show_frame("HomePage")

    def close_edit(self, val):
        if self.edit_window is not None:
            self.canvas.delete(self.edit_window)
            self.edit_window = None
        self.controller.com_right_value = val
        self.init_canvas()

    def open_edit(self, _):
        val = tk.StringVar()
        val.set(self.controller.com_right_value)
        e = tk.Entry(self.canvas, textvariable=val)
        e.bind("<Return>", (lambda event: self.close_edit(e.get())))
        self.edit_window = self.canvas.create_window(370, 110, window=e, height=60)

    def init_canvas(self):
        self.canvas.delete("all")
        self.canvas.create_text(240, 20, text="COM 1", fill='cyan', font='-size 15')
        self.canvas.create_text(100, 45, text="ACT", fill='cyan', font='-size 15')
        self.canvas.create_text(380, 45, text="PRE", fill='cyan', font='-size 15')
        self.canvas.create_rectangle(20, 70, 460, 150, outline='cyan')
        self.canvas.create_text(65, 170, text="SQUELCH", fill='cyan', font='-size 15')
        self.canvas.create_text(32, 195, text="ON", fill='white', font='-size 15')
        self.return_button = self.canvas.create_text(85, 270, text="< RETURN", fill='white', font='-size 20')
        self.draw_arrow()
        self.canvas.create_text(100, 110, text=self.controller.com_left_value, fill='green', font='-size 25')
        self.edit_button = self.canvas.create_text(370, 110, text=self.controller.com_right_value, fill='white',
                                                   font='-size 25')
        self.canvas.tag_bind(self.return_button, "<ButtonPress-1>", self.move_to_home_page)
        self.canvas.tag_bind(self.edit_button, "<ButtonPress-1>", self.open_edit)

    def draw_arrow(self):
        self.canvas.create_line(200, 110, 280, 110, fill='cyan')
        self.canvas.create_line(200, 110, 220, 130, fill='cyan')
        self.canvas.create_line(200, 110, 220, 90, fill='cyan')
        self.canvas.create_line(280, 110, 260, 130, fill='cyan')
        self.canvas.create_line(280, 110, 260, 90, fill='cyan')


if __name__ == "__main__":
    app = RtuApp(className='rtu')
    app.mainloop()
